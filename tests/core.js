
module("Core");

test("Do some stuff", function() {
    ok(true, "True is ok");
    equal(1, 1, "1 is 1");
    equal(1, "1", "1 is \"1\"");
    notEqual(1, 2, "1 is NOT 2");
});

// module("Other Tests", {
//     setup: function() {},
//     teardown: function() {}
// });

// asyncTest("some other stuff", function() {
    
//     CLSY.courses.loadCourses(function(data) {
//         ok(data, "The data is there");
//         start();
//     });

// });

// test("Do more stuff", function() {
//     ok(true, "True is ok");
//     equal(1, 1, "1 is 1");
//     equal(1, "1", "1 is \"1\"");
//     notEqual(1, 2, "1 is NOT 2");
// });

